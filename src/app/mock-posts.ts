import { Post } from './post';

export const POSTS: Post[] = [
	{
	  "title": "ex",
	  "content": "Proident incididunt officia excepteur fugiat aliquip sint culpa laborum veniam officia. Est sint exercitation enim deserunt laboris culpa excepteur eiusmod mollit consectetur. Exercitation dolore dolor veniam pariatur voluptate nostrud Lorem elit exercitation voluptate duis fugiat consequat. Adipisicing enim cupidatat est nisi aliquip sunt non incididunt pariatur labore. Ex magna consectetur eu velit mollit mollit irure sunt mollit. Minim eu veniam reprehenderit tempor.",
	  "loveIts": 0,
	  "created_at": new Date("Wed Aug 21 1991 06:36:14 GMT+0000 (UTC)")
	},
	{
	  "title": "est",
	  "content": "Fugiat est proident officia sint pariatur adipisicing. Qui eiusmod proident elit incididunt. Officia fugiat mollit occaecat est nulla sit elit incididunt sunt sunt Lorem deserunt ex. Amet enim sit fugiat adipisicing est quis eu cillum. Quis anim Lorem reprehenderit proident pariatur ut consequat ullamco culpa proident nostrud incididunt proident.",
	  "loveIts": 0,
	  "created_at": new Date("Wed Jun 26 1996 08:40:45 GMT+0000 (UTC)")
	},
	{
	  "title": "aliquip",
	  "content": "Aliqua proident excepteur aliquip officia cillum exercitation. Non id ex non reprehenderit occaecat in est velit pariatur laboris id Lorem. Dolor est laborum deserunt non qui cupidatat non commodo adipisicing mollit irure sit sit et. Cupidatat aliqua velit laboris id. Adipisicing officia culpa cupidatat ullamco commodo enim velit amet esse.",
	  "loveIts": 0,
	  "created_at": new Date("Wed Jan 05 1977 09:28:27 GMT+0000 (UTC)")
	},
	{
	  "title": "Lorem",
	  "content": "Nulla esse deserunt minim commodo do amet ex ullamco cupidatat. Ullamco pariatur sint est qui magna enim proident. Ipsum esse tempor deserunt id.",
	  "loveIts": 0,
	  "created_at": new Date("Wed Mar 17 1999 19:09:02 GMT+0000 (UTC)")
	}
];